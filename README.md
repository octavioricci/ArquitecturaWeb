Nombre Grupo: BelRi

Integrantes:    Belfanti, Lucas
                Ricci, Octavio

Proyecto:   Estacion-AR

Alcance:    "Estacion-AR" es un software de autogestión de estacionamientos que le permitirá al usuario poder reservar su
            lugar sin necesidad de interactuar personalmente a diferencia de los estacionamientos convencionales.
